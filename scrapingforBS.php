<?php

require_once('phpQuery-onefile.php');
require_once('channels.php');
require_once('database.php');
$pdo=connectDB();

$dberr=fopen("tmp/dberr","a");
ini_set("max_execution_time",10000000);

//ログ情報
$handle = fopen("tmp/log","a");
$errorfile=fopen("tmp/err_log.txt","a");
$urllog=fopen("tmp/urllog","a");

#httpリスポンスを処理するための変数（だけど結局できず使っていない）
$jsessionid="";
$guestId="";
$stationAreaid="";

#データをまとめてデータベースに挿入するための配列
$channelData = array();




//main関数みたいな場所
if(!is_null($argv[1])){
	if($argv[1]==1){
		//requestAllPrefecture();
	}else{
		requestChannelPerPrefectureAday($argv[1]);
	}
}




//使っていない
function firstRequest(){
	global $jsessionid;
	global $guestId;
	global $ch;
	global $handle;
	curl_setopt_array($ch,array(
		CURLOPT_URL=>$url,
		CURLOPT_MAXREDIRS=>5,
		CURLOPT_FOLLOWLOCATION=>true,
		CURLOPT_HEADER=>1,
		CURLOPT_RETURNTRANSFER=>true,
		CURLOPT_HTTPHEADER=>array()
	));
	$r = curl_exec ($ch);

	$info = curl_getinfo ($ch);

	// ヘッダ部分を取得
	$header = substr ($r, 0, $info["header_size"]);
	//$body = substr ($r, -$info["download_content_length"]);
	//fwrite($handle,$header."\n");
	// ボディ部分を取得
	$body = substr ($r, $info["header_size"]);
	// 画面に表示
	$pos=strpos($header,"JSESSION");
	$pos2=strpos($header,";");
	$pos3=strpos($header,"guestId");
	$pos4=strpos($header,";",$pos3);
	$jsessionid=substr($header,$pos+11,$pos2-$pos-11);
	$guestId=substr($header,$pos3+8,$pos4-$pos3-8);

}
//スクレイピングした文字列の処理　全角半角の空白とタブを削除
function mytrim($str) {
    return trim($str, " \t\n\r\0\x0B　"); //← 末尾の空白は全角です。
 }
 //スクレイピングした日付をDateTime型に入れる
function getDates($string){
	$year=date("Y");
	$pos1=strpos($string,"/");
	$pos2=strpos($string,"(");
	$date=substr($string,0,$pos1)."-".substr($string,$pos1+1,$pos2-2-$pos1);
	$pos3=strpos($string,")");
	$pos4=strpos($string,"～");
	$time1=substr($string,$pos3+2,$pos4-1-($pos3+2));
	$pos5=strpos($string,"（");
	$time2=substr($string,$pos4+4,$pos5-2-($pos4+4));
	$date1=$year.'-'.$date.' '.$time1.':00';
	$date2=$year.'-'.$date.' '.$time2.':00';
	$date1=new DateTime($date1);
	$date2=new DateTime($date2);
	return array($date1->format('Y-m-d H:i:s'),$date2->format('Y-m-d H:i:s'));

}
//スクレイピングしたチャンネルをint型に変換
function getChannel($string){
	$pos1=strpos($string,"(Ch.");
 	$pos2=strpos($string,")");
 	if($pos1){
 		$Channel=substr($string,0,$pos1);
		$ChannelNum=intval(substr($string,$pos1+4,$pos2-($pos1+4)));
		return array($Channel,$ChannelNum);
 	}
 	return array(trim($string),99);
	
}
//urlで指定されたhtmlファイルを返す関数
function curlURL($url){
	global $handle;
	 // はじめ
	$ch = curl_init();
	//オプション
	curl_setopt_array($ch,array(
		CURLOPT_URL=>$url,
		CURLOPT_MAXREDIRS=>5,
		CURLOPT_FOLLOWLOCATION=>true,
		CURLOPT_HEADER=>1,
		CURLOPT_RETURNTRANSFER=>true,
		CURLOPT_HTTPHEADER=>array()
	));
	$html =  curl_exec($ch);
	$info=curl_getinfo($ch);
	if($info['http_code']!==200){
		global $errorfile;
		$now=date("Y-m-d H:i:s");
		$err=$now." エラーコード：".$info['http_code'];
		if(!isset($info['http_code'])){
			$err+='空白';
		}
		$err+='\n';
	}
	return $html;
}

//スクレイピングする部分
function detailProgram($url,$number,$retry){
	global $handle ;
	global $pdo;
	global $channels;
	global $argv;
	global $urllog;
	global $dberr;
	global $channelData;
	$channel;
	$channelNum;
	$Program;
	$subdetail;
	$detail;
	$tag;
	$date1;
	$date2;
	$cast;
	$html=curlURL($url);
	$doc=phpQuery::newDocument($html);
	
	$i=0;

	//dlタグのクラスメイbasicTxtの内容を順次取得
	foreach($doc['dl.basicTxt'] as $con){
		$i++;
		for($x=0;$x<4;$x++){
			if($sub1=pq($con)->find("dd:eq($x)")->text()){
				$sub2=pq($con)->find("dd:eq($x)")->find('a')->text();
				$main=trim(str_replace($sub2,'',$sub1))."\n";
				if($main!=='\n'){
					if($x===0){
						$Program=$main;
						$main="番組名：".$main;
					}else if($x===1){
						$ddd=getDates($main);
						$date1=$ddd[0];
						$date2=$ddd[1];
						$main="日付：".$main;
					}else if($x===2){
						if($argv[1]=='bs1' || $argv[1]=='bs2'){
							echo("-----------------".$argv[1]."\n");
							$exp=getChannel($main);
							$channel=$exp[0];
							$channelNum=$exp[1];
						}else{
							echo("-----------------".$argv[1]."\n");
							$channel=trim($main);
							//チャンネルが存在していないのでテキトーに99を設定
							$channelNum=99;
						}
						$main="チャンネル：".$main;
					}else if($x===3){
						$tag=$sub2;
					}
					
				}
			}
		}
		
	}
	$x=0;
	//文字列処理
	foreach ($doc['p.basicTxt'] as $key => $value) {
		
		if($x===0){
			$subdetail=str_replace(array(' ','　','	','\n'),'',pq($value)->text());
			
			
		}else if($x===1){
			$detail=trim(str_replace(array(' ','　','	','\n'),'',pq($value)->text()));
			
		}else if($x===2){
			//fwrite($handle,"出演者：".str_replace(array(' ','　','	','\n'),'',pq($value)->text())."\n");
			$cast = pq($value)->text();
		}
		$x+=1;
	}

	//リトライ処理
	if(is_null($channel)||is_null($channelNum)){
		global $errorfile;
		$now=date("Y-m-d H:i:s");
		fwrite($errorfile,$now." : channel or channelNum is error. URL is $url. \n");
		usleep(5000000);
		echo "retry\n";
		if($retry+1>20){
			return;
		}
		detailProgram($url,$number,$retry+1);
		return;
	}
	//データをchannelDataに挿入
	if($channel&&$Program&&isset($subdetail)&&isset($detail)&&$date1&&$date2){
		if(strtotime($date2)-strtotime($date1)<0){
				$date2=date('Y-m-d H:i:s',strtotime($date2.' +1 day'));
		}
		$cData = array(trim($channel),$channelNum,trim($Program),trim($subdetail),trim($detail),$date1,$date2,$tag,trim($cast));
		array_push($channelData,$cData);
		echo "$date1~$date2:$channel-$Program\n";
	}

	$channel='';
	$channelNum=0;
	$Program='';
	$subdetail='';
	$detail='';
	$date1='';
	$date2='';
	$cast='';

	return;
}




function endDeel($pdo,$number,$handle){
	global $channels;
	global $handle;
	global $errorfile;
	global $urllog;
	global $channelData;
	//sqlite3にデータを挿入
	commitDataToDB($pdo,$channelData,$number);
	//mysqlにデータを挿入
	commitDataToDB(connectmysql(),$channelData,$number);
	//sqlite3の8日以上前のデータを削除
	DeleteEightDaysbefore($pdo,$number);
	//mysqlの8日以上前のデータを削除
	DeleteEightDaysbefore(connectmysql(),$number);
	
	//終了処理
	fwrite($handle,date('Y:m:d H:i:s',strtotime('+9 hour'))." : $number,".$channels[$number]."Success!\n");
	fclose($handle);
	fclose($errorfile);
	fclose($urllog);
}
function requestChannelPerPrefectureAday($number){
	//numberはチャンネル名
	global $handle;
	global $pdo;
	global $channels;
	global $jsessionid;
	global $guestId;
	$httpreq='http://tv.so-net.ne.jp/chart/';
	$json=json_decode(file_get_contents("channel.json"),true);
	//日付指定
	$days=$json['days'];
	foreach($days as $i){
		//urlクエリにいろんな情報を乗っける
		$time=nextDate($i).'0000';
		$url=$httpreq.$number.'.action?span=24'.'&head='.$time;
		echo $url."\n";
		//$url=$httpreq."?span=24?head=".$time."?JSESSIONID=".$jsessionid."?guestId=".$guestId."?stationAreaId=".$number;
		//$url="http://tv.so-net.ne.jp/stationAreaSettingCompleted.action?origin=/chart/23.action&stationAreaId=".$number;
		//fwrite($handle,$url."\n");
		simpleProgram($url,$number);
		//fwrite($handle,date('Y:m:d H:i:s')." $number,$channels[$number] "."Success!\n");

		flush();
		sleep(10);

	}
	endDeel($pdo,$number,$handle);
}

function nextDate($i){
	//日付をテキスト化
	$time=date('Ymd',strtotime('+'.$i.' day'));
	return $time;
}


function simpleProgram($url,$number){
	//いろんな番組が書かれているページをウェブスクレイピング
	global $handle;
	$html=curlURL($url);
	//file_get_contents($url);
	$doc=phpQuery::newDocument($html);
	$int =0;
	foreach($doc[".schedule-link"] as $con){
		//個々の詳細ページをスクレイピングする
		$schedule=pq($con)->attr("href");
		$detail="http://tv.so-net.ne.jp".$schedule;
		// echo "channel url=".$detail;
		$Program=trim(pq($con)->text());
		// echo "\ntext=".$Program."\n";
		detailProgram($detail,$number,0);
		echo "\n";
		usleep(500000);
	}
}

?>
