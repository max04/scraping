<?php 
require_once('database.php');
require_once('channels.php');
$pdo=connectmysql();

//出力したxmlファイルを/home/ba/public_html/television/xmlにまとめて出力する
foreach($channels as $key => $value){
	createxml($pdo,$key);
	for($i=0;$i<8;$i++){
		system('cp xml/'.$channels[$key].$i.'.xml /home/ba/public_html/television/xml');
		// system('cp xml/'.$channels[$key].$i.'.xml /var/www/html/xml/');
	}
}

//日付処理をする関数
function selectDate($i){
	$d = new DateTime();
	$requestDate = new DateTime($d->format('Y-m-d').' 05:00:00');
	$i = $i;
	$requestDate->modify('+'.$i.' days');
	return array($requestDate->format('Y-m-d H:i:s'),$requestDate->modify('+1 days')->format('Y-m-d H:i:s'));
}

//xmlファイルを出力する関数
function createxml($pdo,$number){
	global $channels;
	$json=json_decode(file_get_contents("channel.json"),true);
	$designatedchannel="('".implode("','",$json[$channels[$number]])."')";
	for ($i = 0 ; $i<8;$i++){
		$dom=new DomDocument('1.0','utf-8');
		$dom->formatOutput=true;
		$column=$dom->appendChild($dom->createElement('item'));
		$requestDate = selectDate($i);

		try{
			$sql="SELECT id,channel, channelNum, Program, subdetail, detail, date1, date2, tag,castNames,innerid from ".$channels[$number]." where date2 >= '".$requestDate[0]."' and date1 < '".$requestDate[1]."' and channel in $designatedchannel order by channelNum,date1";
			$st=$pdo->query($sql);
		}catch(PDOException $e){
			die('エラー'.$e->getMessage());
		}
		while($row=$st->fetch()){

			//取得したデータを変数に入れる
			$tag_item=$column->appendChild($dom->createElement('data'));
			$id = $row[0];
			$channel=htmlspecialchars($row[1]);
			$channelNum=htmlspecialchars($row[2]);
			$Program=htmlspecialchars($row[3]);
			$subdetail=htmlspecialchars($row[4]);
			$detail=htmlspecialchars($row[5]);
			$date1=$row[6];
			$date2=$row[7];
			$tag = htmlspecialchars($row[8]);
			$cast = $row[9];
			$innerid = $row[10];

			//xmlファイルの構造を決定
			$tab_id = $tag_item->appendChild($dom->createElement('id',$id));
			$tag_channel=$tag_item->appendChild($dom->createElement('channel',$channel));
			$tag_channelNum=$tag_item->appendChild($dom->createElement('channelNum',$channelNum));
			$tag_Program=$tag_item->appendChild($dom->createElement('Program',$Program));
			$tag_subdetail=$tag_item->appendChild($dom->createElement('subdetail',$subdetail));
			$tag_detail=$tag_item->appendChild($dom->createElement('detail',$detail));
			$tag_date1=$tag_item->appendChild($dom->createElement('date1',$date1));
			$tag_date2=$tag_item->appendChild($dom->createElement('date2',$date2));
			$tag_tag = $tag_item->appendChild($dom->createElement('tag',$tag));
			$tag_cast = $tag_item->appendChild($dom->createElement('castNames',$cast));
			$tag_innerid = $tag_item->appendChild($dom->createElement('innerid',$innerid));

		}
		//xmlファイルを保存
		$dom->saveXML();
		$prefix = "xml/";
		$FileName=$prefix.$channels[$number].$i.'.xml';
		$dom->save($FileName);
		//system('chmod 0777 '.$FileName);exit;
	}

}

?>
