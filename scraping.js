//casperjsを用いてスクレイピングを行うjavascriptファイル

//casperjsを使う指示を出す文
var casper = require('casper').create({clientScripts: ['jquery-3.2.1.min.js']});
//file system（ファイル読み書き)を使う
var fs=require("fs");
//httpステータスコードを得るためのデコーダー
var utils=require('utils');
//jsonファイルを読み取る
var json = require('channel.json');
casper.start(); 
//scraping.shで実装されたものの引数を得る
var channels=[casper.cli.args[0]];
//casperjsの実行する関数を入れる配列、後ろでまとめて実行する
var array=[];
//指定された日付をスクレイピングする
var fetchDay=json['days'];
// var fetchDay=[7];
var get;

//一応ユーザエージェント偽造
casper.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36 ');


fetchDay.forEach(function(day){
	//日付ごとにスクレイピングするための日付処理
	array.push(function(){
		var now=new Date();
		var year=now.getYear();
		var month=now.getMonth()+1;
		if(month<='9'&&month>='1'){
			month='0'+month;
		}
		var date=now.getDate()+day;
		if(date<='9'&&date>='1'){
			date='0'+date;
		}
		if(year<2000){ year+=1900;}
		var time=year+''+month+''+date+'0000';
		var siteurl='http://tv.so-net.ne.jp/chart/23.action?span=24&head='+time;
		casper.thenOpen(siteurl,function(response){
			// utils.dump(response.status);
			if(response.status==308){
				casper.click("a[href^='https://tv']");
			}
		});
		// casper.wait(5000);
	});

	var now=new Date();
	var year=now.getYear();
	var month=now.getMonth()+1;
	var date=now.getDate();
	if(year<2000){ year+=1900;}
	var time=year+''+month+''+date+'0000';
	var c=0;
	var i=0;
	c+=i;
	for(i=0;i<channels.length;i++){
		//テレビ王国の各都道府県にアクセスするための関数
		array.push(function(){
			casper.evaluate(function(i,channels){
				$('#area-selector').val(channels[i]);
				$('#area-selector').trigger('change');
			},c,channels);
		});
		//番組情報取得
		array.push(function(){
			
			get=casper.evaluate(function(){
				var url=new Array();
				$('.schedule-link').each(function(index,element){
						url.push($(this).attr("href"));
				});
				return url;
			});
			// var html=casper.getHTML();
			// fs.write("red.html",html);
			var logtext=now+":"+channels[c]+" is succeeded when it got URLs.\n";
			casper.capture("capture/"+channels[0]+".png")
		});
		//scrapingjs.phpにデータを渡す
		array.push(function(){
			for(var v=0;v<get.length;v++){
				console.log(channels[c]);
				console.log("http://tv.so-net.ne.jp"+get[v]);
				
			}
		});
	}
});
//casperjsを終了する
array.push(function(){
	casper.exit();
});
//arrayに入れた関数を順次実行
array.forEach(function(value){
	casper.then(value);
});
//まとめて実行
casper.run();
