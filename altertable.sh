#!bin/bash

set -e 
if [ -e backup ];then
	echo "backup folder exists"
else
	mkdir backup
fi
cp channel_db.sqlite3 backup
php register.php