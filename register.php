<?php 
require_once("channels.php");
require_once("database.php");

 allDeleteTable(connectDB());
 createTable(connectDB());

allDeleteTable(connectmysql());
createTableformysql(connectmysql());
// altertablev2();

//テーブルを初めて作る時用の関数（挿入されているデータは削除される
function createTable($pdo){
	// global $pdo;
	global $channels;
	foreach($channels as $key => $value){
		try{
			$sql='create table if not exists '.$value.' (
			id integer not null primary key autoincrement,
			channel varchar(50) default null,
			channelNum mediumint default null,
			Program varchar(200) default null,
			subdetail varchar(5000) default null,
			detail varchar(10000) default null,
			tag varchar(100) default null,
			date1 datetime default null,
			date2 datetime default null,
			castNames varchar(5000) default null,
			innerid integer default null);';
			$pdo->query($sql);
		}catch(PDOExeption $e){
			die('エラー:'.$e->getMessage());
		}
	}
}

//mysql用の関数
function createTableformysql($pdo){
	global $channels;
	foreach($channels as $key => $value){
		try{
			$sql='create table if not exists '.$value.' (
			id bigint not null primary key auto_increment,
			channel varchar(50) default null,
			channelNum mediumint default null,
			Program varchar(200) default null,
			subdetail varchar(5000) default null,
			detail varchar(10000) default null,
			tag varchar(100) default null,
			date1 datetime default null,
			date2 datetime default null,
			castNames varchar(5000) default null,
			innerid int default null);';
			$pdo->query($sql);
		}catch(PDOExeption $e){
			die('エラー:'.$e->getMessage());
		}
	}
}

//全テーブルを削除する
function allDeleteTable($pdo){
	global $channels;
	foreach($channels as $key=>$value){
		try{
			$sql='drop table if exists '.$value.';';
			$pdo->query($sql);
		}catch(PDOExeption $e){
			die('エラー：'.$e->getMessage());
		}
	}
}

?>
