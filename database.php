<?php
require_once('channels.php');

//sqlite3用の接続関数
function connectDB(){
	
	try{
		$pdo=new PDO('sqlite:channel_db.sqlite3');
		$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,true);
		print "接続完了";
	}catch(PDOException $e){
		die('エラー：'.$e->getMessage());
	}
	return $pdo;
}

//mysql用の接続関数
function connectmysql(){
	
	global $targetdirectory;
	$json = json_decode(file_get_contents("adminmysql.json"),true);
	$user=$json["user"];
        $pass=$json["pass"];
        $host=$json["host"];
        $name=$json["name"];
        $type=$json["type"];
        $dsn="$type:host=$host;dbname=$name;charset=utf8";
        try{
                $pdo=new PDO($dsn,$user,$pass);
                //$pdo=new PDO('sqlite:channel_db.sqlite3');
                $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,true);
                print "接続完了";
        }catch(PDOException $e){
                die('エラー：'.$e->getMessage());
        }
        return $pdo;
}

//スクレイピングしたデータをデータベースに挿入する関数
function commitDataToDB($pdo,$data,$number,$count=0){

	$dberr=fopen("tmp/dberr","a");
	global $channels;
	$pdo->beginTransaction();
	$json=json_decode(file_get_contents("channel.json"),true);

	echo "書き込み中\n";
	try{
		
		foreach($data as $array){

			$innerid=-1;
			foreach($json[$channels[$number]] as $key => $value){
				if($value==$array[0]){
					$innerid=$key;
					break;
				}
			}


			$query="select count(*) from $channels[$number] where (date1 = '$array[5]' or date2 = '$array[6]') and innerid = $innerid and channel='$array[0]';";
			$result=$pdo->query($query);
			$sql="insert into ".$channels[$number]." (channel,channelNum,Program,subdetail,detail,date1,date2,tag,castNames,innerid) values(:channel,:channelNum,:Program,:subdetail,:detail,:date1,:date2,:tag,:castNames,:innerid);";
			while($row = $result->fetch(PDO::FETCH_COLUMN)){
				if(((int)$row)>0){
					$sql = "update ".$channels[$number]." set channel=:channel,channelNum=:channelNum,Program=:Program,subdetail=:subdetail,detail=:detail,date1=:date1,date2=:date2,tag=:tag,castNames=:castNames,innerid=:innerid where date1 = '".$array[5]."' and channelNum = ".$array[1]." and channel='".$array[0]."';";
				}
			}
			
			$stmt=$pdo->prepare($sql);

			$stmt->bindValue("channel",$array[0],PDO::PARAM_STR);//channel
			$stmt->bindValue("channelNum",$array[1],PDO::PARAM_INT);//channelNum
			$stmt->bindValue("Program",$array[2],PDO::PARAM_STR);//program
			$stmt->bindValue("subdetail",$array[3],PDO::PARAM_STR);//subdetail
			$stmt->bindValue("detail",$array[4],PDO::PARAM_STR);//detail
			$stmt->bindValue("date1",$array[5],PDO::PARAM_STR);//date1
			$stmt->bindValue("date2",$array[6],PDO::PARAM_STR);//date2
			$stmt->bindValue("tag",$array[7],PDO::PARAM_STR);//tag
			$stmt->bindValue("castNames",$array[8],PDO::PARAM_STR);//castNames
			$stmt->bindValue("innerid",$innerid,PDO::PARAM_INT);

			$stmt->execute();
			
		}
		$pdo->commit();
	}catch(PDOException $e){
		$pdo->rollBack();
		$pdo=null;
		fwrite($dberr,date("$number||Y:m:d H:i:s:",strtotime('+9 hour')).':'.$e->getMessage()."\n");
		if($count==20){
			die("count is 20 \n");
		}
	}
	echo "書き込み終了\n";
	fclose($dberr);
}

//同じデータが挿入された時に削除する関数（今はデータ挿入時に上書きするようにしているため大丈夫だとは思うが、念の為実装している）
function DeleteSame($pdo,$num){
	global $channels;
	try{
		$sql = 'delete from '.$channels[$num].' where id not in ( select min_id from (select min(id) min_id from '.$channels[$num].' group by channelNum,date1,date2) tmp);';
		$pdo->query($sql);

	}catch(PDOException $e){
		echo "Error:".$e->getMessage();
	}

}

//8日以上前のデータを削除する関数
function DeleteEightDaysbefore($pdo,$num){
	global $channels;
	try{
		$now=date('Y-m-d H:i:s');
		$before=date('Y-m-d H:i:s',strtotime('-8 day'));
		$sql='delete from '.$channels[$num].' where date1 < "'.$before.'";';
		$stmt=$pdo->prepare($sql);
		$stmt->execute();
	}catch(PDOexception $e){
		die("エラー:".$e->getMessage());
	}

return $pdo;
}

//指定期間のデータを削除する関数
function Deletedesignatedterm($pdo,$num,$day){
	global $channels;
	try{
		$now=date('Y-m-d H:i:s');
		$term=selectDatefordelete($day);
		$sql="delete from $channels[$num] where date2 > '$term[0]' and date1 < '$term[1]';";
		$stmt=$pdo->prepare($sql);
		$stmt->execute();
	}catch(PDOexception $e){
		die("エラー:".$e->getMessage());
	}

return $pdo;
}

//ある日付のデータを削除する関数
function selectDatefordelete($i){
	$d = new DateTime();
	$requestDate = new DateTime($d->format('Y-m-d').' 00:00:00');
	$i = $i;
	$requestDate->modify('+'.$i.' days');
	return array($requestDate->format('Y-m-d H:i:s'),$requestDate->modify('+1 days')->format('Y-m-d H:i:s'));
}

//テーブルのデータを保持したまま一つのカラムを追加する関数
function altertable(){
	$pdo=connectDB();
	$pdo->beginTransaction();
	$dberr=fopen("tmp/dberr","a");
	global $channels;
	try {
		foreach($channels as $key => $value){
			$sql="ALTER TABLE ".$value." ADD COLUMN innerid integer;";
			$stmt=$pdo->prepare($sql);
			$stmt->execute();
		}
		$pdo->commit();
	}catch(PDOexception $e){
		$pdo->rollBack();
		fwrite($dberr,date('Y:m:d H:i:s:',strtotime('+9 hour')).'|mysql|'.$e->getMessage());
		die("error:".$e->getMessage());
	}
}

//テーブルのデータを保持したままテーブル定義を変更する関数
function altertablev2(){
	$pdo=connectDB();
	
	$dberr=fopen("tmp/dberr","a");
	global $channels;
	$pdo->beginTransaction();
	try {
		foreach($channels as $key => $value){
			$value_temp=$value."temp";
			//drop table if exists $value_temp;
			$sql1="ALTER TABLE $value RENAME TO $value_temp;";
			$sql2="
			create table $value (
			id integer not null primary key autoincrement,
			channel varchar(50) default null,
			channelNum mediumint default null,
			Program varchar(200) default null,
			subdetail varchar(5000) default null,
			detail varchar(10000) default null,
			tag varchar(100) default null,
			date1 datetime default null,
			date2 datetime default null,
			castNames varchar(5000) default null,
			innerid integer default null
			);
			";
			$sql3="
			INSERT INTO $value(id,channel,channelNum,Program,detail,tag,date1,date2,castNames) SELECT id,channel,channelNum,Program,detail,tag,date1,date2,castNames from $value_temp;";

			$sql4="DROP TABLE $value_temp;";
			$pdo->query($sql1);
			$pdo->query($sql2);
			$pdo->query($sql3);
			$pdo->query($sql4);
				
		}
		$pdo->commit();
	}catch(PDOexception $e){
		$pdo->rollBack();
		fwrite($dberr,date('Y:m:d H:i:s:',strtotime('+9 hour')).':'.$e->getMessage()."\n");
		die("error:".$e->getMessage()."\n");
	}
	
}

?>
