<?php
require_once('phpQuery-onefile.php');
require_once('channels.php');
require_once('database.php');
//error_reporting(0);

echo date('Y:m:d H:i:s',strtotime('+9 hour'));
$pdo=connectDB();
//init
ini_set("max_execution_time",10000000);

//ログ情報
$handle = fopen("tmp/log","a");
$errorfile=fopen("tmp/err_log.txt","a");
$urllog=fopen("tmp/urllog","a");
$dberr=fopen("tmp/dberr","a");
$isSuccess=false;
$channelData = array();

//scraping.jsを実行し、データを取得
exec('casperjs scraping.js '.$argv[1],$resultData,$result);
$prenum=$resultData[0];

if($result===0) {
	var_dump($resultData);
	$counter=count($resultData);
	$divideChannel=$counter/2;
	echo $divideChannel."\n";
	for($i=0;$i<$divideChannel;$i++){
		$num=$resultData[$i*2].'';
		$url=$resultData[$i*2+1];
		detailProgram($url,$num,0);
		if($prenum!=$num){
			endDeel($pdo,$prenum,$handle);
		}
		$prenum=$num;
	}
	endDeel($pdo,$prenum,$handle);
}


//スクレイピングした文字列の処理　全角半角の空白とタブを削除
function mytrim($str) {
    return trim($str, " \t\n\r\0\x0B　"); //← 末尾の空白は全角です。
 }

 //スクレイピングした日付をDateTime型に入れる
function getDates($string){
	$year=date("Y");
	$pos1=strpos($string,"/");
	$pos2=strpos($string,"(");
	$date=substr($string,0,$pos1)."-".substr($string,$pos1+1,$pos2-2-$pos1);
	$pos3=strpos($string,")");
	$pos4=strpos($string,"～");
	$time1=substr($string,$pos3+2,$pos4-1-($pos3+2));
	$pos5=strpos($string,"（");
	$time2=substr($string,$pos4+4,$pos5-2-($pos4+4));
	$date1=$year.'-'.$date.' '.$time1.':00';
	$date2=$year.'-'.$date.' '.$time2.':00';
	$date1=new DateTime($date1);
	$date2=new DateTime($date2);
	return array($date1->format('Y-m-d H:i:s'),$date2->format('Y-m-d H:i:s'));
}

//スクレイピングしたチャンネルをint型に変換
function getChannel($string){
	$pos1=strpos($string,"(Ch.");
 	$pos2=strpos($string,")");
 	if($pos1){
 		$Channel=substr($string,0,$pos1);
		$ChannelNum=intval(substr($string,$pos1+4,$pos2-($pos1+4)));
		return array($Channel,$ChannelNum);
 	}
 	return array(trim($string),-1);
	
}

//urlで指定されたhtmlファイルを返す関数
function curlURL($url){
	global $handle;
	 // はじめ
	$ch = curl_init();
	//オプション
	curl_setopt_array($ch,array(
		CURLOPT_URL=>$url,
		CURLOPT_MAXREDIRS=>5,
		CURLOPT_FOLLOWLOCATION=>true,
		CURLOPT_HEADER=>1,
		CURLOPT_RETURNTRANSFER=>true,
		CURLOPT_HTTPHEADER=>array()
	));
	$html =  curl_exec($ch);
	$info=curl_getinfo($ch);
	if($info['http_code']!==200){
		global $errorfile;
		$now=date("Y-m-d H:i:s",strtotime('+9 hour'));
		$err=$now." エラーコード：".$info['http_code'];
		if(!isset($info['http_code'])){
			$err+='空白';
		}
		$err+='\n';
		fwrite($errorfile,$err);
	}
	return $html;
}

//スクレイピングする部分
function detailProgram($url,$number,$retry){
	usleep(500000);
	global $handle ;
	global $pdo;
	global $channels;
	global $urllog;
	global $dberr;
	global $channelData;
	$channel;
	$channelNum;
	$Program;
	$subdetail;
	$detail;
	$date1;
	$date2;
	$cast;
	$html=curlURL($url);
	$doc=phpQuery::newDocument($html);
	$i=0;

	//dlタグのクラスメイbasicTxtの内容を順次取得
	foreach($doc['dl.basicTxt'] as $con){
		$i++;
		for($x=0;$x<4;$x++){
			if($sub1=pq($con)->find("dd:eq($x)")->text()){
				$sub2=pq($con)->find("dd:eq($x)")->find('a')->text();
				$main=trim(str_replace($sub2,'',$sub1))."\n";
				if($main!=='\n'){
					if($x===0){
						$Program=$main;
						$main="番組名：".$main;
					}else if($x===1){
						$ddd=getDates($main);
						$date1=$ddd[0];
						$date2=$ddd[1];
						$main="日付：".$main;
					}else if($x===2){
						$exp=getChannel($main);
						$channel=$exp[0];
						$channelNum=$exp[1];
						
						$main="チャンネル：".$main;
						
					}else if($x===3){
						$tag=$sub2;
					}
					
				}
			}
		}
		
	}

	//文字列処理
	$x=0;
	foreach ($doc['p.basicTxt'] as $key => $value) {
		
		if($x===0){
			$subdetail=str_replace(array(' ','　','	','\n'),'',pq($value)->text());
			
			
		}else if($x===1){
			$detail=trim(str_replace(array(' ','　','	','\n'),'',pq($value)->text()));
			
		}else if($x===2){
			//fwrite($handle,"出演者：".str_replace(array(' ','　','	','\n'),'',pq($value)->text())."\n");
			// echo "出演者：".pq($value)->text()."\n";
			$cast = pq($value)->text();
		}
		$x+=1;
	}

	//リトライ処理
	if(is_null($channel)||is_null($channelNum)){
		global $errorfile;
		echo "url is $url \n";
		echo "title is ".$doc['title']->text()."\n";
		foreach($doc['h2'] as $key => $value){
			echo "h2 text is \n".pq($value)->text()."\n";
		}

		$now=date("Y-m-d H:i:s",strtotime('+9 hour'));

		fwrite($errorfile,$now." : channel or channelNum is error. URL is $url. \n");
		usleep(5000000);
		if($retry+1>20){
			return;
		}
		detailProgram($url,$number,$retry+1);
		return;
	}
	
		//データをchannelDataに挿入
	if($channel&&$channelNum&&$Program&&isset($subdetail)&&isset($detail)&&$date1&&$date2){
		if(strtotime($date2)-strtotime($date1)<0){
				$date2=date('Y-m-d H:i:s',strtotime($date2.' +1 day'));
		}
		$cData = array(trim($channel),$channelNum,trim($Program),trim($subdetail),trim($detail),$date1,$date2,$tag,trim($cast));
		array_push($channelData,$cData);
		echo "$channels[$number]||$date1:$channel-$Program\n";
	}

	$channel='';
	$channelNum=0;
	$Program='';
	$subdetail='';
	$detail='';
	$date1='';
	$date2='';
	$cast='';
	
	return;
}

function endDeel($pdo,$number,$handle){
	global $channels;
	global $handle;
	global $errorfile;
	global $urllog;
	global $channelData;
	
	$d = date('Y-m-d H:i:s');
	echo $d."\n";
	
	//sqlite3にデータを挿入
	commitDataToDB($pdo,$channelData,$number);
	//mysqlにデータを挿入
	commitDataToDB(connectmysql(),$channelData,$number);
	$d =date('Y-m-d H:i:s');
	echo "$d:stop\n";
	//sqlite3の8日以上前のデータを削除
	DeleteEightDaysbefore($pdo,$number);
	//mysqlの8日以上前のデータを削除
	DeleteEightDaysbefore(connectmysql(),$number);
	
	//終了処理
	fwrite($handle,date('Y:m:d H:i:s',strtotime('+9 hour'))." : $number,".$channels[$number]."Success!\n");
	fclose($handle);
	fclose($errorfile);
	fclose($urllog);
}







exit(0);?>
