<?php
//Post通信を使って検索条件を受け取り、jsonファイルを返す。



// $targetdirectory=dirname(__FILE__)."/";
$targetdirectory= "/home/ba/scraping/";
// $targetdirectory = "/home/ba/test/scraping/";
require_once($targetdirectory."channels.php");
if (isset($_POST['name']) && isset($_POST['prefecture']) && isset($_POST['bs']) && isset($_POST['DTB'])) {
  $name = $_POST['name'];
  $prefecture = $_POST['prefecture'];
  $orBS = $_POST['bs'];
  $DTB = $_POST['DTB'];
  searchKey($name,$prefecture,$orBS,$DTB);
  // echo "Successfully submitted: " . $name.":prefectureNum= " . $num ;

} else {
	//サンプルデータ
  //echo "Failed";
	searchKey('え','Hokkaido_Sapporo','1','1');
  exit;
}

//検索する関数
function searchKey($key,$prefecture,$orBS,$DTB){
	global $channels;
	global $targetdirectory;
	// $pdo=connectDB();
	$pdo=connectmysql();
	$sql = "";
	$d = new DateTime();
	$searchedDate = $d->format('Y-m-d H:i:s');
	$datequery = "date2 > '$searchedDate' ";
	$json=json_decode(file_get_contents($targetdirectory."channel.json"),true);


	if($orBS == 1 && $DTB == 1){
		//BSと地デジ両方検索
		$col=array_merge($json[$prefecture],$json['bs1'],$json['bs2'],$json['bs3'],$json['bs4']);
		$designatedchannel="('".implode("','",$col)."')";
		$channelquery=" and channel in ".$designatedchannel;
		$datequery=$datequery.$channelquery;
		$sql = "select * from $prefecture where (Program like :key or detail like :key) and ".$datequery;
		$sql1 = "select * from bs1 where (Program like :key or detail like :key) and ".$datequery;
		$sql2 = "select * from bs2 where (Program like :key or detail like :key) and ".$datequery;
		$sql3 = "select * from bs3 where (Program like :key or detail like :key) and ".$datequery;
		$sql4 = "select * from bs4 where (Program like :key or detail like :key) and ".$datequery;
		$sql = $sql." union ".$sql1." union ".$sql2." union ".$sql3." union ".$sql4.";".$datequery;
	}else if($orBS == 1 && $DTB == 0){
		//BSだけ検索
		$col=array_merge($json['bs1'],$json['bs2'],$json['bs3'],$json['bs4']);
		$designatedchannel="('".implode("','",$col)."')";
		$channelquery=" and channel in ".$designatedchannel;
		$datequery=$datequery.$channelquery;
		$sql1 = "select * from bs1 where (Program like :key or detail like :key) and ".$datequery;
		$sql2 = "select * from bs2 where (Program like :key or detail like :key) and ".$datequery;
		$sql3 = "select * from bs3 where (Program like :key or detail like :key) and ".$datequery;
		$sql4 = "select * from bs4 where (Program like :key or detail like :key) and ".$datequery;
		$sql = $sql1." union ".$sql2." union ".$sql3." union ".$sql4.";";
	}else if($orBS == 0 && $DTB == 1){
		//地デジだけ検索
		$designatedchannel="('".implode("','",$json[$prefecture])."')";
		$channelquery=" and channel in ".$designatedchannel;
		$datequery=$datequery.$channelquery;
		$sql = "select * from ".$prefecture." where (Program like :key or detail like :key) and $datequery;" ;
	}
	else {
		//ありえないけど書いておく
		echo "error";
		exit(2);
	}
	
	$stmt = $pdo->prepare($sql);
	$k = "%".$key."%";
	$stmt->bindValue(':key',$k,PDO::PARAM_STR);
	$stmt->execute();
	$result = $stmt->fetchAll();
	//jsonを出力する
	echo json_encode($result);

}

//sqlite3に接続する関数
function connectDB(){
	global $targetdirectory;
	$user='arimax';
	$pass='arimax04';
	$host="localhost";
	$name="channel";
	$type="mysql";
	$dsn="$type:host=$host;dbname=$name;charset=utf8";
	try{
		//$pdo=new PDO($dsn,$user,$pass);
		$pdo=new PDO("sqlite:".$targetdirectory."channel_db.sqlite3");
		$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,true);
	}catch(PDOException $e){
		die('エラー：'.$e->getMessage());
	}
	return $pdo;
}

//mysqlに接続する関数
function connectmysql(){
	global $targetdirectory;
	$json = json_decode(file_get_contents($targetdirectory."adminmysql.json"),true);
	$user=$json["user"];
        $pass=$json["pass"];
        $host=$json["host"];
        $name=$json["name"];
        $type=$json["type"];
        $dsn="$type:host=$host;dbname=$name;charset=utf8";
        try{
                $pdo=new PDO($dsn,$user,$pass);
                //$pdo=new PDO('sqlite:channel_db.sqlite3');
                $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,true);
            
        }catch(PDOException $e){
                die('エラー：'.$e->getMessage());
        }
        return $pdo;
}
?>
